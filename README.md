# ChatApp

#### 介绍
Build a Real Time Chat App With Node.js And Socket.io
本项目来自Youtube上面的一个[视频](https://www.youtube.com/watch?v=rxzOqP9YwmM)，作者的giithub地址为:[https://github.com/WebDevSimplified](https://github.com/WebDevSimplified)，他的[CodeOpen](https://codepen.io)地址是：[https://codepen.io/WebDevSimplified](https://codepen.io/WebDevSimplified)。项目相关介绍参见我的CSDN博客：[使用Nodejs和socket.io创建一个实时的聊天应用](https://blog.csdn.net/ccf19881030/article/details/105696860)

#### 软件架构
本项目采用Node.js和socket.io实现实时的多人网页实时聊天应用。

#### 使用说明

1.  下载项目
```shell
git clone https://github.com/ccf19881030/ChatRoomApp.git
```
2.  安装依赖
```shell
cd ChatRoomApp
npm install
```

#### 安装教程

1.  运行后端
```shell
nodemon server.js
```

2.  运行前端
使用Google或者FireFox等支持Websocket的浏览器打开ChatRoomApp里面的index.html文件，既可以进行多个人的实时聊天。


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
